package movies.importer;

import java.lang.reflect.Array;
import java.util.ArrayList;

public class RemoveDuplicates extends Processor {
    public RemoveDuplicates(String sourceDir, String outputDir)
    {
        super(sourceDir, outputDir, false);
    }

    @Override
    public ArrayList<String> process(ArrayList<String> input)
    {
        ArrayList<String> noDupes = new ArrayList<String>();

        for(String s : input)
        {
            if(!noDupes.contains(s))
            {
                noDupes.add(s);
            }
        }

        return noDupes;
    }
}
