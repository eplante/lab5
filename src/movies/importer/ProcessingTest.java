package movies.importer;

import java.io.IOException;

public class ProcessingTest {
    public static void main(String[] args) throws IOException
    {
        String lpInDir = "E:\\School\\College\\Semester 3\\Programming 3\\lab5\\input";
        String lpOutDir = "E:\\School\\College\\Semester 3\\Programming 3\\lab5\\output";
        String rdOutDir = "E:\\School\\College\\Semester 3\\Programming 3\\lab5\\dupes";
        LowercaseProcessor lp = new LowercaseProcessor(lpInDir, lpOutDir, true);
        RemoveDuplicates rd = new RemoveDuplicates(lpOutDir, rdOutDir);
        lp.execute();
        rd.execute();
    }
}
